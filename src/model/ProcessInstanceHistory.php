<?php
/**
 *+------------------
 * Ingenious
 *+------------------
 * Copyright (c) https://gitee.com/ingenstream/ingenious  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Software Registration Number: 2024SR0694589
 * Official Website: http://www.ingenstream.cn
 */

namespace madong\laravel\wf\model;

use madong\ingenious\enums\ProcessConstEnum;
use madong\ingenious\interface\model\IProcessInstanceHistory;
use madong\laravel\wf\basic\BaseModel;

class ProcessInstanceHistory extends BaseModel implements IProcessInstanceHistory
{
    /**
     * 数据表主键
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * 表名
     *
     * @var string
     */
    protected $table = 'wf_process_instance_history';

    /**
     * 是否自增id
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * 是否指定时间戳
     *
     * @var bool
     */
    public $timestamps = true;

    // 自定义时间戳字段
    const CREATED_AT = 'create_time'; // 自定义创建时间字段
    const UPDATED_AT = 'update_time'; // 自定义更新时间字段

    protected $appends = ['ext', 'form_data','create_date', 'update_date'];

    // 定义字段的数据类型
    protected $casts = [
        'variable' => 'array', // 将 JSON 字段转换为 PHP 数组
    ];

    protected $fillable = [
        'parent_id',
        'process_define_id',
        'state',
        'parent_node_name',
        'business_no',
        'operator',
        'variable',
        'expire_time',
        'create_time',
        'create_user',
        'update_time',
        'update_user',
    ];


    /**
     * 定义访问器
     *
     * @return null
     */
    public function getExtAttribute(): mixed
    {
        return $this->variable ? $this->variable : [];
    }

    /**
     * ID-搜索器
     *
     * @param $query
     * @param $value
     */
    public function scopeId($query, $value)
    {
        if (!empty($value)) {
            $query->where('id', $value);
        }
    }

    /**
     * 父流程ID-搜索器
     *
     * @param $query
     * @param $value
     */
    public function scopeParentId($query, $value)
    {
        if (!empty($value)) {
            $query->where('parent_id', $value);
        }
    }

    /**
     * 流程定义ID-搜索器
     *
     * @param $query
     * @param $value
     */
    public function scopeProcessDefineId($query, $value)
    {
        if ($value) {
            $query->where('process_define_id', $value);
        }
    }

    /**
     *  实例状态搜索器
     *
     * @param $query
     * @param $value
     */
    public function scopeState($query, $value)
    {
        if ($value) {
            $query->where('state', $value);
        }
    }

    /**
     * 父流程依赖节点-搜索器
     *
     * @param $query
     * @param $value
     */
    public function scopeParentNodeName($query, $value)
    {
        if ($value) {
            $query->where('parent_node_name', $value);
        }
    }

    /**
     * 业务编号-搜索器
     *
     * @param $query
     * @param $value
     */
    public function scopeBusinessNo($query, $value)
    {
        if ($value) {
            $query->where('business_no', $value);
        }
    }

    /**
     * 流程发起人-搜索器
     *
     * @param $query
     * @param $value
     */
    public function scopeOperator($query, $value)
    {
        if ($value) {
            $query->where('operator', $value);
        }
    }

    /**
     * 访问器-表单数据
     *
     * @return \stdClass
     */
    public function getFormDataAttribute(): \stdClass
    {
        $formData = new \stdClass();
        $ext      = $this->getAttribute('variable');
        // 过滤以 'f_' 开头的键
        $formDataKeys = array_filter(array_keys($ext), function ($key) {
            return str_starts_with($key, ProcessConstEnum::FORM_DATA_PREFIX->value);
        });
        foreach ($formDataKeys as $key) {
            $formData->{$key} = $ext[$key] ?? '';
        }
        return $formData;
    }

    /**
     * 流程实例-关联父级
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function define(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ProcessDefine::class, 'process_define_id', 'id');
    }

}
