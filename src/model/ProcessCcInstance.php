<?php
/**
 *+------------------
 * Ingenious
 *+------------------
 * Copyright (c) https://gitee.com/ingenstream/ingenious  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Software Registration Number: 2024SR0694589
 * Official Website: http://www.ingenstream.cn
 */

namespace madong\laravel\wf\model;

use madong\ingenious\interface\model\IProcessCcInstance;
use madong\laravel\wf\basic\BaseModel;

/**
 * 流程实例抄送-模型
 *
 * @author Mr.April
 * @since  1.0
 */
class ProcessCcInstance extends  BaseModel implements IProcessCcInstance
{

    /**
     * 数据表主键
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * 表名
     *
     * @var string
     */
    protected $table = 'wf_process_cc_instance';

    /**
     * 是否自增id
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * 是否指定时间戳
     *
     * @var bool
     */
    public $timestamps = true;

    // 自定义时间戳字段
    const CREATED_AT = 'create_time'; // 自定义创建时间字段
    const UPDATED_AT = 'update_time'; // 自定义更新时间字段

    protected $appends = ['create_date', 'update_date'];

    protected $fillable = [
        'id',
        'process_instance_id',
        'process_task_id',
        'actor_id',
        'state',
        'create_time',
        'update_time',
        'create_user',
        'update_user',
    ];

    public function instance(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ProcessInstance::class, 'process_instance_id', 'id');
    }

    public function define(): \Illuminate\Database\Eloquent\Relations\HasOneThrough
    {
        return $this->hasOneThrough(ProcessDefine::class, ProcessInstance::class, 'id', 'id', 'process_instance_id', 'process_define_id');
    }

}
