<?php
/**
 *+------------------
 * Ingenious
 *+------------------
 * Copyright (c) https://gitee.com/ingenstream/ingenious  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Software Registration Number: 2024SR0694589
 * Official Website: http://www.ingenstream.cn
 */

namespace madong\laravel\wf\model;

use madong\ingenious\interface\model\IProcessSurrogate;
use madong\laravel\wf\basic\BaseModel;

class ProcessSurrogate extends BaseModel implements IProcessSurrogate
{

    /**
     * 数据表主键
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * 表名
     *
     * @var string
     */
    protected $table = 'wf_process_surrogate';

    /**
     * 是否自增id
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * 是否指定时间戳
     *
     * @var bool
     */
    public $timestamps = true;

    // 自定义时间戳字段
    const CREATED_AT = 'create_time'; // 自定义创建时间字段
    const UPDATED_AT = 'update_time'; // 自定义更新时间字段

    protected $appends = ['type_id', 'display_name', 'version', 'create_date', 'update_date', 'start_date', 'end_date'];

    protected $fillable = [
        'id',
        'process_define_id',
        'operator',
        'surrogate',
        'start_time',
        'end_time',
        'enabled',
        'create_time',
        'create_by',
        'update_time',
        'update_by',
    ];

    public function getStartDateAttribute(): ?string
    {
        if ($this->getAttribute('start_time')) {
            try {
                $timestamp = $this->getRawOriginal('start_time');
                if (empty($timestamp)) {
                    return null;
                }
                return date('Y-m-d H:i:s', $timestamp);
            } catch (\Exception $e) {
                return null;
            }
        }
        return null;
    }

    public function getEndDateAttribute(): ?string
    {
        if ($this->getAttribute('end_time')) {
            try {
                $timestamp = $this->getRawOriginal('end_time');
                if (empty($timestamp)) {
                    return null;
                }
                return date('Y-m-d H:i:s', $timestamp);
            } catch (\Exception $e) {
                return null;
            }
        }
        return null;
    }

    /**
     * 定义访问器
     *
     * @return null
     */
    public function getTypeIdAttribute(): mixed
    {
        return $this->define ? $this->define->type_id : '';
    }

    /**
     * 定义访问器
     *
     * @return null
     */
    public function getDisplayNameAttribute(): mixed
    {
        return $this->define ? $this->define->display_name : '';
    }

    /**
     * 定义访问器
     *
     * @return null
     */
    public function getVersionAttribute(): mixed
    {
        return $this->define ? $this->define->version : '';
    }

    /**
     * ID-搜索器
     *
     * @param $query
     * @param $value
     */
    public function scopeId($query, $value)
    {
        if ($value) {
            $query->where('id', $value);
        }
    }

    /**
     * 流程名称-搜索器
     *
     * @param $query
     * @param $value
     */
    public function scopeProcessName($query, $value)
    {
        if ($value) {
            $query->where('process_name', $value);
        }
    }

    /**
     * 授权人-搜索器
     *
     * @param $query
     * @param $value
     */
    public function scopeOperator($query, $value)
    {
        if ($value) {
            $query->where('operator', $value);
        }
    }

    /**
     * 代理人-搜索器
     *
     * @param $query
     * @param $value
     */
    public function scopeSurrogate($query, $value)
    {
        if ($value) {
            $query->where('surrogate', $value);
        }
    }

    /**
     * 启用状态-搜索器
     *
     * @param $query
     * @param $value
     */
    public function scopeEnabled($query, $value)
    {
        if ($value !== '') {
            $query->where('enabled', $value);
        }
    }

    /**
     * 流程实例一对一流程定义表
     */
    public function define(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(ProcessDefine::class, 'id', 'process_define_id');
    }

}
