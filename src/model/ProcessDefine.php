<?php
/**
 *+------------------
 * Ingenious
 *+------------------
 * Copyright (c) https://gitee.com/ingenstream/ingenious  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Software Registration Number: 2024SR0694589
 * Official Website: http://www.ingenstream.cn
 */

namespace madong\laravel\wf\model;

use madong\laravel\wf\basic\BaseModel;
use madong\ingenious\interface\model\IProcessDefine;

/**
 * 流程定义-模型
 *
 * @author Mr.April
 * @since  1.0
 */
class ProcessDefine extends BaseModel implements IProcessDefine
{

    /**
     * 数据表主键
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * 表名
     *
     * @var string
     */
    protected $table = 'wf_process_define';

    /**
     * 是否自增id
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * 是否指定时间戳
     *
     * @var bool
     */
    public $timestamps = true;

    // 自定义时间戳字段
    const CREATED_AT = 'create_time'; // 自定义创建时间字段
    const UPDATED_AT = 'update_time'; // 自定义更新时间字段

    protected $casts = [
        'content' => 'array',
    ];

    protected $fillable = [
        'id',
        'type_id',
        'icon',
        'name',
        'display_name',
        'description',
        'enabled',
        'is_active',
        'content',
        'version',
        'create_time',
        'create_user',
        'update_time',
        'update_user',
        'delete_time',
    ];

    protected $appends = ['create_date', 'update_date'];

    /**
     * 唯一编码
     *
     * @param $query
     * @param $value
     */
    public function scopeName($query, $value)
    {
        if (!empty($value)) {
            $query->where('name', $value);
        }
    }

    /**
     *  显示名称搜索器
     *
     * @param $query
     * @param $value
     */
    public function scopeDisplayName($query, $value)
    {
        if (!empty($value)) {
            $query->whereLike('display_name', '%' . $value . '%');
        }
    }

    /**
     * 状态搜索器
     *
     * @param $query
     * @param $value
     */
    public function scopeEnabled($query, $value)
    {
        if ($value !== '') {
            $query->where('enabled', $value);
        }
    }

    /**
     *  描述搜索器
     *
     * @param $query
     * @param $value
     */
    public function scopeDescription($query, $value)
    {
        if ($value) {
            $query->whereLike('description', '%' . $value . '%');
        }
    }

    /**
     * 是否活动流程
     *
     * @param $query
     * @param $value
     */
    public function scopeIsActive($query, $value)
    {
        if (!empty($value)) {
            $query->where('is_active', $value);
        }
    }

    /**
     * 分类搜索器
     *
     * @param $query
     * @param $value
     */
    public function scopeTypeId($query, $value)
    {
        if ($value) {
            $query->where('type_id', $value);
        }
    }

    /**
     * 流程版本-搜索器
     *
     * @param $query
     * @param $value
     */
    public function scopeVersion($query, $value)
    {
        if (!empty($value)) {
            $query->where('version', $value);
        }
    }

}
