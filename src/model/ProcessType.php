<?php
/**
 *+------------------
 * Ingenious
 *+------------------
 * Copyright (c) https://gitee.com/ingenstream/ingenious  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Software Registration Number: 2024SR0694589
 * Official Website: http://www.ingenstream.cn
 */

namespace madong\laravel\wf\model;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use madong\ingenious\interface\model\IProcessType;
use madong\laravel\wf\basic\BaseModel;

class ProcessType extends BaseModel implements IProcessType
{
    use SoftDeletes;

    /**
     * 数据表主键
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * 表名
     *
     * @var string
     */
    protected $table = 'wf_process_type';

    protected $dates = []; // 指定 deleted_at 字段为日期类型

    protected $appends = ['pid_name', 'create_date', 'update_date'];

    /**
     * 是否自增id
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * 是否指定时间戳
     *
     * @var bool
     */
    public $timestamps = true;

    // 自定义时间戳字段
    const CREATED_AT = 'create_time'; // 自定义创建时间字段
    const UPDATED_AT = 'update_time'; // 自定义更新时间字段
    const DELETED_AT = 'delete_time';  // 指定软删除字段的名称

    protected $fillable = [
        'id',
        'pid',
        'icon',
        'name',
        'sort',
        'enabled',
        'create_time',
        'update_time',
        'create_user',
        'update_user',
        'create_user',
        'delete_time',
        'remark',
    ];

    /**
     * 定义访问器
     *
     * @return null
     */
    public function getPidNameAttribute(): mixed
    {
        return $this->parent ? $this->parent->name : '顶级';
    }

    public function scopePid($query, $value)
    {
        if (!empty($value)) {
            $query->where('pid', $value);
        }
    }

    /**
     * 类型名称
     *
     * @param $query
     * @param $value
     */
    public function scopeName($query, $value)
    {

        if (!empty($value)) {
            $query->whereLike('name', '%' . $value . '%');
        }
    }

    /**
     * 是否删除搜索器
     *
     * @param $query
     * @param $value
     */
    public function scopeDeleteTime($query, $value)
    {
        if (!empty($value)) {
            $query->where('delete_time', $value);
        } else {
            $query->where('delete_time', null);
        }
    }

    /**
     * 状态搜索器
     *
     * @param $query
     * @param $value
     */
    public function scopeEnabled($query, $value)
    {
        if ($value !== null) {
            $query->where('enabled', $value);
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function parent(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(ProcessType::class, 'id', 'pid');
    }

    /**
     * 类型关联流程定义
     *
     * @return HasMany
     */
    public function items(): HasMany
    {
        return $this->hasMany(ProcessDefine::class, 'type_id', 'id')->where('enabled', 1);
    }

    public function history(): HasMany
    {
        return $this->hasMany(ProcessDefine::class, 'type_id', 'id')->where('enabled', 1);
    }

}
