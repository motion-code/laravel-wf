<?php
/**
 *+------------------
 * Ingenious
 *+------------------
 * Copyright (c) https://gitee.com/ingenstream/ingenious  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Software Registration Number: 2024SR0694589
 * Official Website: http://www.ingenstream.cn
 */

namespace madong\laravel\wf\model;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use madong\laravel\wf\basic\BaseModel;
use madong\ingenious\interface\model\IProcessForm;

/**
 * 流程定义-模型
 *
 * @author Mr.April
 * @since  1.0
 */
class ProcessForm extends BaseModel implements IProcessForm
{

    /**
     * 数据表主键
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * 表名
     *
     * @var string
     */
    protected $table = 'wf_process_form';

    /**
     * 是否自增id
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * 是否指定时间戳
     *
     * @var bool
     */
    public $timestamps = true;

    // 自定义时间戳字段
    const CREATED_AT = 'create_time'; // 自定义创建时间字段
    const UPDATED_AT = 'update_time'; // 自定义更新时间字段
    const DELETED_AT = 'delete_time';  // 指定软删除字段的名称

    protected $appends = ['type_name', 'create_date', 'update_date'];

    protected $fillable = [
        'id',
        'name',
        'display_name',
        'description',
        'type_id',
        'icon',
        'is_deployed',
        'create_time',
        'create_user',
        'update_time',
        'update_user',
        'delete_time',
        'remark',
    ];

    protected function serializeDate(\DateTimeInterface $date): string
    {
        return $date->format('Y-m-d H:i:s'); // 或者其他您想要的格式
    }

    /**
     * 定义访问器
     *
     * @return null
     */
    public function getTypeNameAttribute(): mixed
    {
        return $this->types ? $this->types->name : null;
    }

    public function scopeId($query, $value)
    {
        if ($value) {
            $query->where('id', $value);
        }
    }

    public function scopeTypeId($query, $value)
    {
        if (!empty($value)) {
            $query->where('type_id', $value);
        }
    }

    public function scopeNotId($query, $value)
    {
        if ($value) {
            $query->where('id', '<>', $value);
        }
    }

    /**
     * 唯一编码
     *
     * @param $query
     * @param $value
     */
    public function scopeName($query, $value)
    {
        if ($value) {
            $query->where('name', $value);
        }
    }

    /**
     * 显示名称搜索器
     *
     * @param $query
     * @param $value
     */
    public function scopeDisplayName($query, $value)
    {
        if ($value) {
            $query->whereLike('display_name', '%' . $value . '%');
        }
    }

    /**
     * 流程定义描述搜索器
     *
     * @param $query
     * @param $value +
     */
    public function scopeDescription($query, $value)
    {
        if ($value) {
            $query->whereLike('description', '%' . $value . '%');
        }
    }

    /**
     * 定义管理一对一流程类型表
     *
     * @return HasOne
     */
    public function types(): HasOne
    {
        return $this->hasOne(ProcessType::class, 'id', 'type_id')->where('enabled', 1);
    }

    /**
     * 定义关联一对多历史表
     *
     * @return HasMany
     */
    public function history(): HasMany
    {
        return $this->hasMany(ProcessFormHistory::class, 'process_form_id', 'id');
    }

    /**
     * 删除设计及其关联的历史记录
     *
     * @return bool
     */
    public function deleteWithHistory(): bool
    {
        $this->history()->delete();
        return $this->delete();
    }

}
