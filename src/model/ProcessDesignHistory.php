<?php
/**
 *+------------------
 * Ingenious
 *+------------------
 * Copyright (c) https://gitee.com/ingenstream/ingenious  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Software Registration Number: 2024SR0694589
 * Official Website: http://www.ingenstream.cn
 */

namespace madong\laravel\wf\model;

use madong\laravel\wf\basic\BaseModel;
use madong\ingenious\interface\model\IProcessDesignHistory;

class ProcessDesignHistory extends BaseModel implements IProcessDesignHistory
{

    /**
     * 数据表主键
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * 表名
     *
     * @var string
     */
    protected $table = 'wf_process_design_history';

    /**
     * 是否自增id
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * 是否指定时间戳
     *
     * @var bool
     */
    public $timestamps = false;

    // 自定义时间戳字段
    const CREATED_AT = 'create_time'; // 自定义创建时间字段
    const UPDATED_AT = 'update_time'; // 自定义更新时间字段

    protected $appends = ['create_date', 'update_date'];

    protected $casts = [
        'content' => 'array',
    ];

    protected $fillable = [
        'id',
        'process_design_id',
        'content',
        'create_time',
        'create_user',
        'version',
    ];

    /**
     * ID  搜索器
     *
     * @param $query
     * @param $value
     */
    public function scopeId($query, $value)
    {
        if ($value) {
            $query->where('id', $value);
        }
    }

    /**
     * 流程设计ID搜索器
     *
     * @param $query
     * @param $value
     */
    public function scopeProcessDesignId($query, $value)
    {
        if (!empty($value)) {
            $query->where('process_design_id', $value);
        }
    }

}
