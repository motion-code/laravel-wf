<?php
/**
 *+------------------
 * Ingenious
 *+------------------
 * Copyright (c) https://gitee.com/ingenstream/ingenious  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Software Registration Number: 2024SR0694589
 * Official Website: http://www.ingenstream.cn
 */

namespace madong\laravel\wf\model;

use madong\laravel\wf\basic\BaseModel;
use madong\ingenious\interface\model\IProcessDefineFavorite;

/**
 * 流程定义-模型
 *
 * @author Mr.April
 * @since  1.0
 */
class ProcessDefineFavorite extends BaseModel implements IProcessDefineFavorite
{

    /**
     * 数据表主键
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * 表名
     *
     * @var string
     */
    protected $table = 'wf_process_define_favorite';

    /**
     * 是否自增id
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * 是否指定时间戳
     *
     * @var bool
     */
    public $timestamps = true;

    // 自定义时间戳字段
    const CREATED_AT = 'create_time'; // 自定义创建时间字段
    const UPDATED_AT = 'update_time'; // 自定义更新时间字段

    protected $appends = ['create_date', 'update_date'];

    protected $fillable = [
        'id',
        'user_id',
        'process_define_id',
        'create_time',
        'created_by',
        'remark',
    ];

    public function define(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ProcessDefine::class, 'process_define_id', 'id');
    }

}
