<?php
/**
 *+------------------
 * madong
 *+------------------
 * Copyright (c) https://gitee.com/motion-code  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Official Website: http://www.madong.tech
 */

namespace madong\laravel\wf\dao;

use madong\ingenious\libs\utils\ArrayHelper;
use madong\laravel\wf\basic\BaseDao;
use madong\laravel\wf\model\ProcessDefineFavorite;

class ProcessDefineFavoriteDao extends BaseDao
{

    protected function setModel(): string
    {
        return ProcessDefineFavorite::class;
    }

//    public function getList($where, $field = ['*'], $page, $limit, $order = '', $with = []): array
//    {
//        $result = $this->getModel()->with(['define'])
//            ->where($where)
//            ->orderBy('create_time', 'desc')
//            ->paginate($limit, ['*'], 'page', $page);
//        var_dump($result);
//
//        return [
//            'items' => $result->items(),   // 当前页的数据
//            'total' => $result->total(), // 总条数
//        ];
//    }

    public function getList($where = [], $field = ['*'], $page = 0, $limit = 0, $order = '', $with = []): array
    {
        $map0  = ArrayHelper::filterArray(ArrayHelper::paramsFilter($where, [
            ['user_id', ''],
            ['process_define_id', ''],
        ]));
        $map1  = ArrayHelper::filterArray(ArrayHelper::paramsFilter($where, [
            ['name', ''],
            ['display_name', ''],
        ]));
        $query = $this->getModel()->with(['define'])
            ->where($map0)
            ->whereHas('define', function ($query) use ($map1) {
                if (!empty($map1)) {
                    $query->where($map1);
                }
            })
            ->orderBy('create_time', 'desc')
            ->paginate($limit, ['*'], 'page', $page);
        return [
            $query->items(),
            $query->total(),
        ];
    }
}
