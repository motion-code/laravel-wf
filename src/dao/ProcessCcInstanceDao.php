<?php
/**
 *+------------------
 * madong
 *+------------------
 * Copyright (c) https://gitee.com/motion-code  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Official Website: http://www.madong.tech
 */

namespace madong\laravel\wf\dao;

use madong\ingenious\libs\utils\ArrayHelper;
use madong\laravel\wf\basic\BaseDao;
use madong\laravel\wf\model\ProcessCcInstance;


/**
 * @author Mr.April
 * @since  1.0
 */
class ProcessCcInstanceDao extends BaseDao
{

    protected function setModel(): string
    {
        return ProcessCcInstance::class;
    }

    public function getList($where = [], $field = ['*'], $page = 0, $limit = 0, $order = '', $with = []): array
    {
        $map0  = ArrayHelper::filterArray(ArrayHelper::paramsFilter($where, [
            ['actor_id', ''],
            ['state', 1],
            ['process_instance_id', ''],
        ]));
        $map1  = ArrayHelper::filterArray(ArrayHelper::paramsFilter($where, [
            ['business_no'],
            ['operator']
        ]));
        $map2  = ArrayHelper::filterArray(ArrayHelper::paramsFilter($where, [
            ['name'],
            ['display_name'],
        ]));
        $query = $this->getModel()->with(['instance', 'define'])
            ->where($map0)
            ->whereHas('instance', function ($query) use ($map1) {
                if (!empty($map1)) {
                    $query->where($map1);
                }
            })
            ->whereHas('define', function ($query) use ($map2) {
                if (!empty($map2)) {
                    $query->where($map2);
                }
            })
            ->orderBy('create_time', 'desc')
            ->paginate($limit, ['*'], 'page', $page);
        return [
            $query->items(),
            $query->total(),
        ];
    }
}
