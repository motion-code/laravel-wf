<?php
/**
 *+------------------
 * madong
 *+------------------
 * Copyright (c) https://gitee.com/motion-code  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Official Website: http://www.madong.tech
 */

namespace madong\laravel\wf\dao;

use madong\laravel\wf\basic\BaseDao;
use madong\ingenious\libs\utils\ArrayHelper;
use madong\laravel\wf\model\ProcessSurrogate;

class ProcessSurrogateDao extends BaseDao
{

    protected function setModel(): string
    {
        return ProcessSurrogate::class;
    }

    public function getList($where = [], $field = ['*'], $page = 0, $limit = 0, $order = '', $with = []): array
    {
        $map0 = ArrayHelper::filterArray(ArrayHelper::paramsFilter($where, [
            ['surrogate'],
            ['operator'],
            ['process_define_id'],
            ['enabled'],
        ]));
        $map1  = ArrayHelper::filterArray(ArrayHelper::paramsFilter($where, [
            ['name'],
            ['display_name'],
        ]));
        $query = $this->getModel()->with(['define'])
            ->where($map0)
            ->whereHas('define', function ($query) use ($map1) {
                if (!empty($map1)) {
                    $query->where($map1);
                }
            })
            ->orderBy('create_time', 'desc')
            ->paginate($limit, ['*'], 'page', $page);
        return [
            $query->items(),
            $query->total(),
        ];
    }

}
