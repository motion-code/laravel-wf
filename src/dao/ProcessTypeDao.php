<?php
/**
 *+------------------
 * madong
 *+------------------
 * Copyright (c) https://gitee.com/motion-code  All rights reserved.
 *+------------------
 * Author: Mr. April (405784684@qq.com)
 *+------------------
 * Official Website: http://www.madong.tech
 */

namespace madong\laravel\wf\dao;

use madong\laravel\wf\basic\BaseDao;
use madong\laravel\wf\model\ProcessType;

class ProcessTypeDao extends BaseDao
{

    protected function setModel(): string
    {
        return ProcessType::class;
    }
}
